Test task for www.makves.ru
Модифицировать пример http://recharts.org/en-US/examples/SimpleLineChart

Раскрасить все участки графиков на которых z-score > 1 в красный цвет. Цвет точек графика должен совпадать с цветом участка.

Справка по расчету z-score: https://en.wikipedia.org/wiki/Standard_score
