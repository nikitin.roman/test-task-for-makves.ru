import React from 'react';
import {maxValue, elements} from "./constants";
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';


function App() {
    const data = []
    let sum = 0
    let sumD = 0

    for (let i = 0; i < elements; i++) {
        let x = Math.floor(Math.random() * maxValue)
        sum += x
        data.push({"x": x, "name": `dot ${i}`})
    }

    const M = sum / data.length

    for (let i of data) {
        sumD += Math.pow((i.x - M), 2)
    }

    const D = sumD / data.length
    const S = Math.pow(D, 1 / 2)
    const limit = M+S
    const data_copy = []

    for (let i = 0; i < data.length-1; i++) {
        //Z >= 1 при x - M >= S
        if (data[i].x < limit && data[i+1].x < limit){
            data_copy.push({"blue": data[i].x, "name": data[i].name})
        }
        if (data[i].x < limit && data[i+1].x > limit){
            data_copy.push({"blue": data[i].x, "name": data[i].name})
            data_copy.push({"blue": limit, "red": limit})
        }
        if (data[i].x > limit && data[i+1].x < limit){
            data_copy.push({"red": data[i].x, "name": data[i].name})
            data_copy.push({"blue": limit, "red": limit})
        }
        if (data[i].x > limit && data[i+1].x > limit){
            data_copy.push({"red": data[i].x, "name": data[i].name})
        }
    }


    return (
        <div className="App">
            <LineChart width={1500} height={700} data={data_copy}
                       margin={{top: 50, right: 30, left: 20, bottom: 5}}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Tooltip/>
                <Legend/>
                <Line type="monotone" dataKey="red" stroke="#FF0000"/>
                <Line type="monotone" dataKey="blue" stroke="#0000ff"/>
            </LineChart>
        </div>
    );
}

export default App;
